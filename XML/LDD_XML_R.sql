CREATE TABLE Personne (
    pseudo VARCHAR2(255) PRIMARY KEY,
    email VARCHAR2(255) UNIQUE NOT NULL,
    prenom VARCHAR2(255),
    nom VARCHAR2(255),
    date_naissance DATE,
    lien_photo VARCHAR2(255),
    numero VARCHAR2(255),
    mot_de_passe VARCHAR2(255) NOT NULL,
    date_inscription DATE NOT NULL,
    date_derniere_connexion DATE,
    est_superadmin NUMBER(1) NOT NULL,
    adresse XMLType,
    Formation XMLTYPE,
    Competences XMLType
);
/

CREATE TABLE Groupe (
    nom VARCHAR2(255) PRIMARY KEY,
    description VARCHAR2(500),
    createur VARCHAR2(255) REFERENCES Personne(pseudo) NOT NULL,
    parent VARCHAR2(255) REFERENCES Groupe(nom),
    date_creation DATE NOT NULL,
    suppression VARCHAR2(255) REFERENCES Personne(pseudo)
);
/

CREATE TABLE Adhesion (
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    nom VARCHAR2(255) REFERENCES Groupe(nom),
    date_demande DATE NOT NULL,
    adhesion_validee NUMBER(1) NOT NULL,
    PRIMARY KEY(pseudo, nom)
);
/

CREATE TABLE Administration (
    nom VARCHAR2(255) REFERENCES Groupe(nom),
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    PRIMARY KEY(pseudo, nom)
);
/

CREATE TABLE Bannissement (
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    nom VARCHAR2(255) REFERENCES Groupe(nom),
    banni_par VARCHAR2(255) REFERENCES Personne(pseudo) NOT NULL,
    debut_bannissement DATE NOT NULL,
    fin_bannissement DATE,
    PRIMARY KEY(pseudo, nom)
);
/

CREATE TABLE Lien (
    description VARCHAR2(255) PRIMARY KEY
);
/
CREATE TABLE Relation (
    personne1 VARCHAR2(255) REFERENCES Personne(pseudo),
    personne2 VARCHAR2(255) REFERENCES Personne(pseudo),
    type_lien VARCHAR2(255) REFERENCES Lien(description),
    PRIMARY KEY (personne1, personne2)
);
/



CREATE TABLE Entreprise (
    siren INTEGER PRIMARY KEY,
    nom VARCHAR2(255) NOT NULL,
    secteur VARCHAR2(255),
    nb_employes INTEGER,
    pays VARCHAR2(255),
    logo_url VARCHAR2(255)
);
/

CREATE TABLE Experience (
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    siren INTEGER REFERENCES Entreprise(siren),
    debut DATE NOT NULL,
    fin DATE,
    poste VARCHAR2(255) NOT NULL,
    description VARCHAR2(500),
    PRIMARY KEY (pseudo, siren, debut)
);
/

CREATE TABLE Type (
    nom VARCHAR2(255) PRIMARY KEY
);
/

CREATE TABLE Message (
	numero INTEGER PRIMARY KEY,
    auteur VARCHAR2(255) REFERENCES Personne(pseudo) NOT NULL,
    date_publication TIMESTAMP NOT NULL,
    groupe VARCHAR2(255) REFERENCES Groupe(nom) NOT NULL,
    titre_et_contenu XMLType NOT NULL,
    est_commentable NUMBER(1) NOT NULL,
    supprime NUMBER(1) NOT NULL,
    type VARCHAR2(255) REFERENCES Type(nom) NOT NULL,
    parent INTEGER REFERENCES Message(numero),
    date_derniere_modification DATE,
	UNIQUE(auteur, date_publication)
);
/

CREATE TABLE Aime (
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    message INTEGER REFERENCES Message(numero),  
    PRIMARY KEY(pseudo, message)
);
/

CREATE TABLE Dossier (
    nom VARCHAR2(255) PRIMARY KEY,
    description VARCHAR2(500)
);
/

CREATE TABLE Tags (
    nom VARCHAR2(255) REFERENCES Dossier(nom),
    message INTEGER REFERENCES Message(numero), 
    PRIMARY KEY(nom, message)
);
/