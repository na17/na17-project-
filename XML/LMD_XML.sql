-- Données de test

-- test de la table Personne
-- Personne(pseudo, info_personne, info_site)

INSERT INTO Personne VALUES (
  'Numero1', 
  'numero1@email.com', 
  'Lucas', 
  'Dziurzik', 
  TO_DATE('1974-05-02', 'YYYY-MM-DD'), 
  NULL,
  NULL, 
  'MotDePasseTresSecurise1', 
  TO_DATE('2012-05-02', 'YYYY-MM-DD'), 
  TO_DATE('2019-01-01', 'YYYY-MM-DD'), 
  0,
  XMLType('<adresse> 
  <numero> 28 </numero> 
  <nomrue> rue de la paix </nomrue>
  <codePostal> 60200</codePostal>
  <ville> Compiègne </ville>
  <pays> France </pays> 
  </adresse>'), 
  XMLType('
  <formation>
  <nom_formation> IHM et interculturalité </nom_formation>
  <nom_etablissement> Pierre et Marie Curie UPMC </nom_etablissement>
  <niveau> L1 </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree>1</duree>
  <mention> Bien </mention>
  <description> Comment traduire proprement une interface graphique ? </description>
  <commentaire> rien à déclarer </commentaire>
  </formation>'), 
  XMLType('
 <competences>
  <competence> 
    <nom> POO </nom>
    <description>programmation modulaire en java et C++</description>
    <niveau>5</niveau>
    <commentaire> implémentation des design patterns les plus communs</commentaire>
  </competence>
 </competences>
 ')

  );/

INSERT INTO Personne VALUES (
  'CompiZoo', 
  'compizoo@email.com', 
  'Martin', 
  'Belair', 
  TO_DATE('1989-07-02', 'YYYY-MM-DD'), 
  null, 
  null, 
  'MotDePasseTresSecurise5', 
  TO_DATE('2014-05-19', 'YYYY-MM-DD'), 
  TO_DATE('2019-01-07', 'YYYY-MM-DD'), 
  0, 
  XMLType('<adresse> 
  <numero> 17 </numero> 
  <nomrue> avenue des paquerettes </nomrue>
  <codePostal> 13000</codePostal>
  <ville> Marseille </ville>
  <pays> France </pays> 
  </adresse>'),
  XMLType('
<formation>
  <nom_formation> inforatique - base de donn?es </nom_formation>
  <nom_etablissement> UTC Amiens </nom_etablissement>
  <niveau> DUT  </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree>2</duree>
  <mention> Bien </mention>
  <description> Tout sur les BDD </description>
  <commentaire> rien à déclarer </commentaire>
</formation>'), 
 XMLType('
 <competences>
  <competence> 
    <nom> Changer une barette de RAM</nom>
    <description> blabla</description>
    <niveau>2</niveau>
    <commentaire> c''est très dur</commentaire>
  </competence>
 </competences>
 ')

  );

INSERT INTO Personne VALUES (
  'Potiron', 
  'potiron@email.com', 
  'Louis', 
  'Potiron', 
  TO_DATE('1995-04-02', 'YYYY-MM-DD'), 
  null, 
  null, 
  'MotDePasseIndevinable', 
  TO_DATE('2014-12-20', 'YYYY-MM-DD'), 
  TO_DATE('2019-04-04', 'YYYY-MM-DD'), 
  1,  
  XMLType('<adresse> 
  <numero> 5 </numero> 
  <nomrue> rue des mendiants </nomrue>
  <codePostal> 52159</codePostal>
  <ville> Tristemonde </ville>
  <pays> France </pays> 
  </adresse>'), 
  XMLType('
  <formation>
    <nom_formation> Puce microfluidique </nom_formation>
    <nom_etablissement> Paris XI </nom_etablissement>
    <niveau> Master 2 </niveau>
    <debut_suivi> 2005 </debut_suivi>
    <secteur> Informatique </secteur>
    <duree>3</duree>
    <mention> Bien </mention>
    <description> Tout sur les BDD </description>
    <commentaire> rien à déclarer </commentaire>
  </formation>'), 
   XMLType('
 <competences>
  <competence> 
    <nom> Multithreading en C </nom>
    <description> Création de processus légers pour lancer plusieurs taches en simultané</description>
    <niveau>3</niveau>
    <commentaire> nécessite des connaissances préalables en C</commentaire>
  </competence>
  <competence> 
    <nom> C++ </nom>
    <description> Programmation orienté objet</description>
    <niveau>3</niveau>
    <commentaire> Avoir déjà programmé</commentaire>
  </competence>
 </competences>
 ')
  );/

INSERT INTO Personne VALUES (
  'La_Mouche', 
  'bzz@email.org', 
  'Maya', 
  'Dupontel', 
  To_DATE('1998-05-31', 'YYYY-MM-DD'), 
  null, 
  null, 
  'BzzbzbzzZZbzzbZbz', 
  TO_DATE('2016-09-22', 'YYYY-MM-DD'), 
  TO_DATE('2019-04-17', 'YYYY-MM-DD'), 
  1, 
  XMLType('<adresse> 
  <numero> 1 </numero> 
  <nomrue> rue du professeur Camille Lian </nomrue>
  <codePostal> 89520 </codePostal>
  <ville> Treigny </ville>
  <pays> France </pays> 
  </adresse>'),
  XMLType('
<formation>
  <nom_formation> Ingénieur </nom_formation>
  <nom_etablissement> UTT </nom_etablissement>
  <niveau> BAC+5 </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree>3</duree>
  <mention> Bien </mention>
  <description> génie informatique </description>
  <commentaire> c''était génial </commentaire>
</formation>'), 
 XMLType('
 <competences>
  <competence> 
    <nom> Installer Linux </nom>
    <description>CrÃ©ation d''une clÃ© bootable et installation de linux n''importe quelle distribution</description>
    <niveau>4</niveau>
    <commentaire> </commentaire>
  </competence>
  <competence>
    <nom>Changer une barette de RAM</nom>
    <description>blabla</description>
    <niveau>5</niveau>
    <commentaire>blabla</commentaire>
  </competence>
 </competences>
 ')
  );


-- test de la table Message

INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux', 'Numero1', NULL, TO_DATE('2010-01-01', 'YYYY-MM-DD'), NULL);
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('JQuery', 'Le laisser mourrir en paix.', 'Potiron', NULL, TO_DATE('2017-01-31', 'YYYY-MM-DD'), NULL);

INSERT INTO Type Values ('article');
INSERT INTO Type Values ('commentaire');
INSERT INTO Type VALUES ('requete');
INSERT INTO Type VALUES ('news');
INSERT INTO Type VALUES ('URL');



INSERT INTO Message VALUES (700, 'Numero1', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux',
XMLType(
'<Message>
	<Titre>installer linux</Titre>
	<Contenu>explique comment installer linux sur son ordinateur</Contenu>
</Message>')
, 1, 0, 'article', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
/

INSERT INTO Message VALUES(750, 'Potiron', TO_DATE('2014-03-10', 'YYYY-MM-DD'), 'JQuery'
,XMLType( '<Message><Titre>introduction</Titre> <Contenu>explique comment installer linux sur son ordinateur</Contenu></Message>'),
1, 0, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
/


INSERT INTO Message VALUES (860, 'Numero1', TO_DATE('2019-03-09', 'YYYY-MM-DD'), 'JQuery',
XMLType(
'<Message>
	<Titre>Y a quelqu un????</Titre>
	<Contenu>je suis vraiment tout seul.....</Contenu>
</Message>')
, 1, 0, 'article', NULL, TO_DATE('2019-05-12', 'YYYY-MM-DD'));
/

-- requêtes 

-- afficher les coordonnées de chaque membre du réseau : nom, prenom, pseudo, adresse
SET SERVEROUTPUT ON;
DECLARE
    CURSOR c IS
        SELECT p.pseudo, p.prenom, p.nom, p.adresse.EXTRACT('/adresse').GETSTRINGVAL() FROM Personne p;
    v_pseudo Personne.pseudo%TYPE;
    v_nom Personne.nom%TYPE;
    v_prenom Personne.prenom%TYPE;
    v_adresse VARCHAR(32767);
BEGIN
    DBMS_OUTPUT.PUT_LINE('** Liste des membres du réseau **');
    OPEN c;
    LOOP
        FETCH c INTO v_pseudo, v_nom, v_prenom, v_adresse;
        EXIT WHEN c%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('- ' || INITCAP(TRIM(v_pseudo)) || ' : ' || INITCAP(TRIM(v_prenom)) || 
        ' ' || INITCAP(TRIM(v_nom)) || chr(10) || v_adresse);
    END LOOP;
END;
/

--afficher les compétences de chaque membre du réseau
SET SERVEROUTPUT ON;
DECLARE
    CURSOR c IS
        SELECT p.pseudo, p.prenom, p.nom, p.Competences.EXTRACT('/competences/competence/nom').GETSTRINGVAL() FROM Personne p;
    v_pseudo Personne.pseudo%TYPE;
    v_nom Personne.nom%TYPE;
    v_prenom Personne.prenom%TYPE;
    v_competences VARCHAR(32767);
BEGIN
    DBMS_OUTPUT.PUT_LINE('** Compétences des membres du réseau **' || chr(10) );
    OPEN c;
    LOOP
        FETCH c INTO v_pseudo, v_nom, v_prenom, v_competences;
        EXIT WHEN c%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('- ' || INITCAP(TRIM(v_pseudo)) || ' : ' || INITCAP(TRIM(v_prenom)) || 
        ' ' || INITCAP(TRIM(v_nom)) || chr(10) || v_competences);
    END LOOP;
END;
/


--afficher les experts du réseau (niveau pour une compétence = {4, 5})
SET SERVEROUTPUT ON;
DECLARE
    CURSOR c IS
        SELECT p.pseudo, p.prenom, p.nom, p.Competences.EXTRACT('/competences/competence[niveau>3]/nom').GETSTRINGVAL() FROM Personne p;
    v_pseudo Personne.pseudo%TYPE;
    v_nom Personne.nom%TYPE;
    v_prenom Personne.prenom%TYPE;
    v_competences VARCHAR(32767);
BEGIN
    OPEN c;
    LOOP
        FETCH c INTO v_pseudo, v_nom, v_prenom, v_competences;
        EXIT WHEN c%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('- ' || INITCAP(TRIM(v_pseudo)) || ' : ' || INITCAP(TRIM(v_prenom)) || 
        ' ' || INITCAP(TRIM(v_nom)) || chr(10) || v_competences);
    END LOOP;
END;
/


--afficher les membres qui ont suivi une formation de plus de 2 ans. Note : on peut trier au niveau applicatif les gens qui ont effectivement suivi une formation de plus de 2 ans
-- ou bien mettre les infos personnelles dans le meme document que competence et formation
SET SERVEROUTPUT ON;
DECLARE
    CURSOR c IS
        SELECT p.prenom, p.nom, p.Formation.EXTRACT('/formation[duree>2]/nom_formation').GETSTRINGVAL() FROM Personne p;
    v_nom Personne.nom%TYPE;
    v_prenom Personne.prenom%TYPE;
    v_formation VARCHAR(32767);
BEGIN
    OPEN c;
    LOOP
        FETCH c INTO v_nom, v_prenom, v_formation;
        EXIT WHEN c%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('- '|| INITCAP(TRIM(v_prenom)) || 
        ' ' || INITCAP(TRIM(v_nom)) || chr(10) || v_formation);
    END LOOP;
END;
/


-- selectionner les titres, contenus, auteurs et heure de poste de tous les messages

SET SERVEROUTPUT ON;
DECLARE
    CURSOR c IS
        SELECT m.auteur, m.date_publication, m.titre_et_contenu.EXTRACT('/Message/Titre').GETSTRINGVAL() AS titre_message, m.titre_et_contenu.EXTRACT('/Message/Contenu').GETSTRINGVAL() AS contenu_message FROM Message m;
    v_auteur Message.auteur%TYPE;
    v_date Message.date_publication%TYPE;
    v_titre VARCHAR(200);
    v_contenu VARCHAR(20000);
BEGIN
    OPEN c;
    LOOP
        FETCH c INTO v_auteur, v_date, v_titre, v_contenu;
        EXIT WHEN c%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('- '|| INITCAP(TRIM(v_auteur)) || 
        ' ' || INITCAP(TRIM(v_date)) || chr(10) || v_titre || chr(10) || v_contenu);
    END LOOP;
END;
/
