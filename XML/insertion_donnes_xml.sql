-- Données de test

-- test de la table Personne
-- Personne(pseudo, info_personne, info_site)

INSERT INTO Personne VALUES (
  'Numero1', 
  'numero1@email.com', 
  'Lucas', 
  'Dziurzik', 
  TO_DATE('1974-05-02', 'YYYY-MM-DD'), 
  NULL,
  NULL, 
  'MotDePasseTresSecurise1', 
  TO_DATE('2012-05-02', 'YYYY-MM-DD'), 
  TO_DATE('2019-01-01', 'YYYY-MM-DD'), 
  0,
  XMLType('<adresse> 
  <numero> 28 </numero> 
  <nomrue> rue de la paix </nomrue>
  <codePostal> 60200</codePostal>
  <ville> Compiègne </ville>
  <pays> France </pays> 
  </adresse>'), 
  XMLType('
  <formation>
  <nom_formation> IHM et interculturalité </nom_formation>
  <nom_etablissement> Pierre et Marie Curie UPMC </nom_etablissement>
  <niveau> L1 </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree> 1 an </duree>
  <mention> Bien </mention>
  <description> Comment traduire proprement une interface graphique ? </description>
  <commentaire> rien à déclarer </commentaire>
  </formation>'), 
  XMLType('
 <competences>
  <competence> 
    <nom> POO </nom>
    <description>programmation modulaire en java et C++</description>
    <niveau>5</niveau>
    <commentaire> implémentation des design patterns les plus communs</commentaire>
  </competence>
 </competences>
 ')

  );/

INSERT INTO Personne VALUES (
  'CompiZoo', 
  'compizoo@email.com', 
  'Martin', 
  'Belair', 
  TO_DATE('1989-07-02', 'YYYY-MM-DD'), 
  null, 
  null, 
  'MotDePasseTresSecurise5', 
  TO_DATE('2014-05-19', 'YYYY-MM-DD'), 
  TO_DATE('2019-01-07', 'YYYY-MM-DD'), 
  0, 
  XMLType('<adresse> 
  <numero> 17 </numero> 
  <nomrue> avenue des paquerettes </nomrue>
  <codePostal> 13000</codePostal>
  <ville> Marseille </ville>
  <pays> France </pays> 
  </adresse>'),
  XMLType('
<formation>
  <nom_formation> inforatique - base de donn?es </nom_formation>
  <nom_etablissement> UTC Amiens </nom_etablissement>
  <niveau> DUT  </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree> 2 ans </duree>
  <mention> Bien </mention>
  <description> Tout sur les BDD </description>
  <commentaire> rien à déclarer </commentaire>
</formation>'), 
 XMLType('
 <competences>
  <competence> 
    <nom> Changer une barette de RAM</nom>
    <description> blabla</description>
    <niveau>2</niveau>
    <commentaire> c''est très dur</commentaire>
  </competence>
 </competences>
 ')

  );

INSERT INTO Personne VALUES (
  'Potiron', 
  'potiron@email.com', 
  'Louis', 
  'Potiron', 
  TO_DATE('1995-04-02', 'YYYY-MM-DD'), 
  null, 
  null, 
  'MotDePasseIndevinable', 
  TO_DATE('2014-12-20', 'YYYY-MM-DD'), 
  TO_DATE('2019-04-04', 'YYYY-MM-DD'), 
  1,  
  XMLType('<adresse> 
  <numero> 5 </numero> 
  <nomrue> rue des mendiants </nomrue>
  <codePostal> 52159</codePostal>
  <ville> Tristemonde </ville>
  <pays> France </pays> 
  </adresse>'), 
  XMLType('
  <formation>
    <nom_formation> Puce microfluidique </nom_formation>
    <nom_etablissement> Paris XI </nom_etablissement>
    <niveau> Master 2 </niveau>
    <debut_suivi> 2005 </debut_suivi>
    <secteur> Informatique </secteur>
    <duree> 3 ans </duree>
    <mention> Bien </mention>
    <description> Tout sur les BDD </description>
    <commentaire> rien à déclarer </commentaire>
  </formation>'), 
   XMLType('
 <competences>
  <competence> 
    <nom> Multithreading en C </nom>
    <description> Création de processus légers pour lancer plusieurs taches en simultané</description>
    <niveau>3</niveau>
    <commentaire> nécessite des connaissances préalables en C</commentaire>
  </competence>
  <competence> 
    <nom> C++ </nom>
    <description> Programmation orienté objet</description>
    <niveau>3</niveau>
    <commentaire> Avoir déjà programmé</commentaire>
  </competence>
 </competences>
 ')
  );/

INSERT INTO Personne VALUES (
  'La_Mouche', 
  'bzz@email.org', 
  'Maya', 
  'Dupontel', 
  To_DATE('1998-05-31', 'YYYY-MM-DD'), 
  null, 
  null, 
  'BzzbzbzzZZbzzbZbz', 
  TO_DATE('2016-09-22', 'YYYY-MM-DD'), 
  TO_DATE('2019-04-17', 'YYYY-MM-DD'), 
  1, 
  XMLType('<adresse> 
  <numero> 1 </numero> 
  <nomrue> rue du professeur Camille Lian </nomrue>
  <codePostal> 89520 </codePostal>
  <ville> Treigny </ville>
  <pays> France </pays> 
  </adresse>'),
  XMLType('
<formation>
  <nom_formation> Ingénieur </nom_formation>
  <nom_etablissement> UTT </nom_etablissement>
  <niveau> BAC+5 </niveau>
  <debut_suivi> 2005 </debut_suivi>
  <secteur> Informatique </secteur>
  <duree> 3 ans </duree>
  <mention> Bien </mention>
  <description> génie informatique </description>
  <commentaire> c''était génial </commentaire>
</formation>'), 
 XMLType('
 <competences>
  <competence> 
    <nom> Installer Linux </nom>
    <description>CrÃ©ation d''une clÃ© bootable et installation de linux n''importe quelle distribution</description>
    <niveau>4</niveau>
    <commentaire> </commentaire>
  </competence>
  <competence>
    <nom>Changer une barette de RAM</nom>
    <description>blabla</description>
    <niveau>5</niveau>
    <commentaire>blabla</commentaire>
  </competence>
 </competences>
 ')
  );


-- test de la table Message

INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux', 'Numero1', NULL, TO_DATE('2010-01-01', 'YYYY-MM-DD'), NULL);
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('JQuery', 'Le laisser mourrir en paix.', 'Potiron', NULL, TO_DATE('2017-01-31', 'YYYY-MM-DD'), NULL);

INSERT INTO Type Values ('article');
INSERT INTO Type Values ('commentaire');
INSERT INTO Type VALUES ('requete');
INSERT INTO Type VALUES ('news');
INSERT INTO Type VALUES ('URL');



INSERT INTO Message VALUES (700, 'Numero1', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux',
XMLType(
'<Message>
	<Titre>installer linux</Titre>
	<Contenu>explique comment installer linux sur son ordinateur</Contenu>
</Message>')
, 1, 0, 'article', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
/

INSERT INTO Message VALUES(750, 'Potiron', TO_DATE('2014-03-10', 'YYYY-MM-DD'), 'JQuery'
,XMLType( '<Message><Titre>introduction</Titre> <Contenu>explique comment installer linux sur son ordinateur</Contenu></Message>'),
1, 0, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
/


INSERT INTO Message VALUES (860, 'Numero1', TO_DATE('2019-03-09', 'YYYY-MM-DD'), 'JQuery',
XMLType(
'<Message>
	<Titre>Y a quelqu un????</Titre>
	<Contenu>je suis vraiment tout seul.....</Contenu>
</Message>')
, 1, 0, 'article', NULL, TO_DATE('2019-05-12', 'YYYY-MM-DD'));
/

