CREATE or REPLACE Type competenceT AS OBJECT (
nom VARCHAR2(255),
description VARCHAR2(255),
expertise NUMBER(1)
--CHECK(expertise >=0 AND expertise <= 5)
);
/


CREATE TABLE Competence OF competenceT (PRIMARY KEY(nom), description NOT NULL, expertise NOT NULL);
/



CREATE Type refCompetence AS OBJECT (
    refC REF competenceT
);
/




CREATE TYPE ListrefCompetence AS TABLE OF refCompetence;
/






CREATE TYPE FormationT AS OBJECT (
    nom VARCHAR2(255),
    niveau VARCHAR2(255),
    secteur VARCHAR2(255),
    duree NUMBER(11),
    description VARCHAR2(500)
);
/

CREATE TYPE SuiviDeFormationT AS OBJECT (
  nom_formation REF FormationT,
  debut_suivi DATE,
  specialite VARCHAR2(255),
  mention VARCHAR2(20),
  commentaire VARCHAR2(500)
);
/

CREATE TABLE SuiviDeFormation OF SuiviDeFormationT (
  debut_suivi NOT NULL  
);
/

CREATE TYPE SuiviRefT AS OBJECT (
  suivi REF SuiviDeFormationT
);
/

CREATE TABLE SuiviListe OF SuiviRefT;
/




CREATE Type adresse AS OBJECT (
numero NUMBER(4),
rue VARCHAR2(255),
ville VARCHAR2(255),
code_postal NUMBER(5),
pays VARCHAR2(255)
);
/




CREATE Type numero AS TABLE OF NUMBER(10);
/

DROP TYPE PersonneT;

CREATE OR REPLACE TYPE PersonneT AS OBJECT(
    pseudo VARCHAR2(255),
    email VARCHAR2(255),
    prenom VARCHAR2(255),
    nom VARCHAR2(255),
    date_naissance DATE,
    lien_photo VARCHAR2(255),
    numero_tel numero,
    mot_de_passe VARCHAR2(255),
    date_inscription DATE,
    date_derniere_connexion DATE,
    est_superadmin NUMBER(1),
    competences ListrefCompetence,
    adresse_personne adresse,
    formations SuiviListe
    );
/

CREATE TABLE PERSONNE OF PERSONNET (
    PRIMARY KEY (pseudo),
    email UNIQUE NOT NULL,
    mot_de_passe NOT NULL,
    date_inscription NOT NULL,
    est_superadmin NOT NULL
)
NESTED TABLE competences STORE AS Personne_competence,
NESTED TABLE numero_tel STORE AS Personne_numero;
/
show errors;

CREATE OR REPLACE Type GroupeT AS OBJECT (
    nom VARCHAR2(255),
    description VARCHAR2(500),
    Createur REF PersonneT,
    groupe_parent REF GroupeT,
    date_creation DATE,
    suppression REF PersonneT
);
/
show errors;

CREATE TABLE Groupe OF GroupeT(
  PRIMARY KEY(nom),
  SCOPE FOR (Createur) IS Personne,
  SCOPE FOR (groupe_parent) IS Groupe,
  SCOPE FOR (suppression) IS Personne 
);
/


CREATE Type TypeMsgT AS OBJECT(
	nom VARCHAR2(255) 
);
/

CREATE TABLE Typemsg OF TypemsgT(
  Primary KEY (nom)
);
/

CREATE TYPE MessageT AS OBJECT (
    numero NUMBER,
    auteur REF PersonneT,
    groupe REF GroupeT,
    date_publication TIMESTAMP,
    titre VARCHAR(60),
    contenu VARCHAR(300),
    est_commentable NUMBER(1),
    supprime NUMBER(1),
    type REF TypeMsgT,
    parent REF MessageT,
    date_derniere_modification TIMESTAMP
);
/

CREATE TABLE Message OF MessageT (
  PRIMARY KEY(numero),
  SCOPE FOR (auteur) IS Personne,
  SCOPE FOR (groupe) IS Groupe,
  SCOPE FOR (type) IS TypeMsg,
  SCOPE FOR (parent) IS Message,
  est_commentable NOT NULL,
  contenu NOT NULL,
  type NOT NULL,
  groupe NOT NULL,
  supprime NOT NULL,
  auteur NOT NULL,
  date_publication NOT NULL
);
/

CREATE TABLE Aime (
    pseudo REF PersonneT,
    SCOPE FOR (pseudo) IS Personne,
    message REF MessageT,
    SCOPE FOR (message) IS Message
);
/

CREATE TABLE Adhesion (
    pseudo REF PersonneT,
    SCOPE FOR (pseudo) IS Personne,
    nom_groupe REF GroupeT,
    SCOPE FOR (nom_groupe) IS Groupe,
    date_demande DATE NOT NULL,
    adhesion_validee NUMBER(1) NOT NULL
);
/

CREATE TABLE Administration (
    pseudo REF PersonneT,
    SCOPE FOR (pseudo) IS Personne,
    nom_groupe REF GroupeT,
    SCOPE FOR (nom_groupe) IS Groupe
);
/

CREATE TABLE Bannissement (
    pseudo REF PersonneT,
    SCOPE FOR (pseudo) IS Personne,
    nom_groupe REF GroupeT,
    SCOPE FOR (nom_groupe) IS Groupe,
    banni_par REF PersonneT,
    SCOPE FOR (banni_par) IS Personne,
    debut_bannissement DATE NOT NULL,
    fin_bannissement DATE
);
/


CREATE TABLE Lien (
    description VARCHAR2(255) PRIMARY KEY
);
/

CREATE TABLE Relation (
    personne1 REF PersonneT,
    SCOPE FOR (personne1) IS Personne,
    personne2 REF PersonneT,
    SCOPE FOR (personne2) IS Personne,
    type_lien VARCHAR2(255) REFERENCES Lien(description)
);
/

CREATE Type DossierT AS OBJECT (
    nom VARCHAR2(255),
    description VARCHAR2(500)
);
/

CREATE TABLE Dossier OF DossierT(
  PRIMARY KEY (nom)
);
/

CREATE TABLE Tags (
    nom REF GroupeT,
    SCOPE FOR (nom) IS Groupe,
    message REF MessageT,
    SCOPE FOR (message) IS Message
);
/


CREATE TYPE EntrepriseT AS OBJECT (
    siren NUMBER(11),
    nom VARCHAR2(255),
    secteur VARCHAR2(255),
    nb_employes NUMBER(11),
    pays VARCHAR2(255),
    logo_url VARCHAR2(255),
    adresse_entrepise adresse
);
/

CREATE TABLE Entreprise OF EntrepriseT(
  PRIMARY KEY (siren),
  nom NOT NULL
);
/

CREATE TABLE Experience (
    pseudo VARCHAR2(255) REFERENCES Personne(pseudo),
    siren NUMBER(11) REFERENCES Entreprise(siren),
    debut DATE NOT NULL,
    fin DATE,
    poste VARCHAR2(255) NOT NULL,
    description VARCHAR2(500),
    PRIMARY KEY (pseudo, siren, debut)
);
/