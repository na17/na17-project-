/*select 'drop table '||table_name||' cascade constraints;' from user_tables;*/


CREATE TYPE CompetenceT AS OBJECT(
    nom VARCHAR2(100),
    description VARCHAR2(500),
    expertise NUMBER(1)
);
/
CREATE TABLE COMPETENCE OF CompetenceT(
    nom PRIMARY KEY,
    CHECK (expertise < 5 AND expertise >0)
);
/
CREATE TYPE ListRefCompetenceT AS TABLE OF CompetenceT;
/

CREATE TYPE suiviDeFormationT AS OBJECT(
    nom_competence REF CompetenceT,
    --SCOPE FOR (nom_competence) IS COMPETENCE,
    debut_suivi DATE,
    specialite VARCHAR2(255),
    mention VARCHAR2(255),
    commentaire VARCHAR2(500)
);
/

CREATE TYPE Adresse AS OBJECT(
    numero INTEGER,
    rue VARCHAR2(255),
    ville VARCHAR2(255),
    code_postal NUMBER(5),
    pays VARCHAR2(255)
);
/

CREATE TYPE EntrepriseT AS OBJECT(
    siren INTEGER,
    nom VARCHAR2(255),
    secteur VARCHAR2(255),
    nb_employes NUMBER(5),
    pays VARCHAR2(255),
    logo_url VARCHAR2(500),
    adresse_entreprise Adresse
);
/

CREATE TABLE Entreprise OF EntrepriseT(siren PRIMARY KEY);
/

CREATE TABLE Personne(
    pseudo VARCHAR2(255) PRIMARY KEY,
    email VARCHAR2(255),
    prenom VARCHAR2(255),
    nom VARCHAR2(255),
    date_naissance DATE,
    lien_photo VARCHAR2(500),
    mot_de_passe VARCHAR2(50),
    numero NUMBER(10)
);
/


/************** ajout pas encore testé ************************/
--Groupe (#nom : String, description : String, createur => Personne, parent => Groupe, date_creation : Date, suppression => Personne) avec createur, date_creation NOT NULL

CREATE TABLE Groupe(
    nom VARCHAR2(255) PRIMARY KEY,
    description VARCHAR2(500),
    createur REFERENCES Personne,
    parent REFERENCES Groupe,
    date_creation DATE,
    suppression REFERENCES Personne
);
/

--Type TypemsgT : <nom : string>
--Typemsg de TypemsgT (#nom)

CREATE TYPE TypemsgT AS OBJECT(
    nom VARCHAR2(255),
    descriptif_du_type VARCHAR2(500)
);
/

CREATE TABLE TYPEmsg OF TypemsgT(
    nom PRIMARY KEY
);
/

CREATE TABLE Message(
    numero NUMBER(6) PRIMARY KEY,
    auteur REFERENCES PERSONNE NOT NULL,
    Groupe REFERENCES Groupe,
    date_publication TIMESTAMP,
    titre VARCHAR2(255),
    contenu VARCHAR2(1000) NOT NULL,
    est_commentable NUMBER(1),
    supprime NUMBER(1) NOT NULL,
    type REF TypemsgT NOT NULL,
    parent REFERENCES Message,
    date_derniere_modification TIMESTAMP,
    UNIQUE(auteur, date_publication)
);
/

CREATE TABLE AIME (
    pseudo REFERENCES Personne,
    message REFERENCES Message,
    PRIMARY KEY (pseudo, message)
);
/

CREATE TABLE Adhesion(
    groupe REFERENCES Groupe,
    pseudo REFERENCES Personne,
    adhesion_validee NUMBER(1) CHECK (adhesion_validee BETWEEN 0 and 1) NOT NULL,
    date_demande DATE NOT NULL,
    PRIMARY KEY(groupe, pseudo)
);
/


CREATE TABLE Administration(
    groupe REFERENCES Groupe,
    pseudo REFERENCES Personne,
    PRIMARY KEY(groupe, pseudo)
);
/

CREATE TABLE Banissement(
    groupe REFERENCES Groupe,
    pseudo REFERENCES Personne,
    banni_par REFERENCES Personne,
    debut_banissement DATE NOT NULL,
    fin_banissement DATE,
    PRIMARY KEY(groupe, pseudo)
);
/

CREATE TABLE relation(
    personne1 REFERENCES Personne,
    personne2 REFERENCES Personne,
    type_lien VARCHAR2(50) NOT NULL,
    PRIMARY KEY(personne1, personne2)
);
/

CREATE TABLE Competences_personne(
    pseudo REFERENCES Personne PRIMARY KEY,
    competences ListRefCompetenceT
)
NESTED TABLE competences STORE AS liste_competences_personnes;
/

CREATE TYPE DossierT AS OBJECT(
    nom VARCHAR2(100),
    description VARCHAR2(500)
);
/
CREATE TABLE Dossier OF DossierT(
    nom PRIMARY KEY
);
/

CREATE TABLE Tag(
    message REFERENCES Message PRIMARY KEY,
    dossier REF DossierT
);
/


CREATE TABLE Experience(
    pseudo REFERENCES Personne,
    debut DATE,
    fin DATE,
    entreprise REF EntrepriseT,
    poste VARCHAR2(255) NOT NULL,
    description VARCHAR2(500),
    PRIMARY KEY(pseudo, debut)
);
/


/****************** SCRIPT DESTRUCTION TABLES / TYPES ds le bon bon ordre *********************/
/*
DROP TABLE PERSONNE;
DROP TABLE COMPETENCE;
DROP TYPE SuiviDeFormationT;
DROP TYPE EntrepriseT;
DROP TYPE ADRESSE;
DROP TYPE CompetenceT;
DROP TABLE AIME;
____________________
drop table PERSONNE cascade constraints;
drop table GROUPE cascade constraints;
drop table MESSAGE cascade constraints;
drop table ADHESION cascade constraints;
drop table ADMINISTRATION cascade constraints;
drop table BANISSEMENT cascade constraints;
drop table RELATION cascade constraints;
drop table COMPETENCES_PERSONNE cascade constraints;
drop table TAG cascade constraints;
drop table EXPERIENCE cascade constraints;
DROP TYPE ListRefCompetencet;
DROP TYPE COMPETENCET;
DROP TABLE ENtreprise;
DROP type ENtrepriseT;
DROP type ENtrepriseT;
DROP table dossier;
DROP TABLE Typemsg;
DROP TYPE TypemsgT;
DROP TYPE TypemsgT;
DROP TYPE DOssierT;

*/
/************************************/

