/******************************* insertion de données pour montrer l'interet du relationnel objet identification **************************/

--on va chercher à affecter des experiences à une personne, pour faire des Insertion de références par OID

INSERT INTO Personne VALUES('eloi', 'eloi.juszczak@gmail.com', 'eloi', 'juszczak', TO_DATE('14-11-1998', 'DD-MM-YYYY'), NULL, 'azerty', NULL);

DECLARE 
refI REF EntrepriseT;

BEGIN
INSERT INTO Entreprise VALUES(00000001, 'le roi de l''info', 'informatique', 15, 'France', null, Adresse(10, 'rue de la paix', 'Troyes', 10000, 'France'));


INSERT INTO Entreprise VALUES(00000002, 'nestle', 'agro-alimentaire', 300, 'France', null, Adresse(1, 'rue de la liberté', 'Compiègne', 60200, 'France'));


INSERT INTO Entreprise VALUES(00000003, 'Microsoft', 'informatique', 1500, 'USA', null, Adresse(10, 'rue de la sillicon valley', 'Los Angeles', 90000, 'USA'));


SELECT REF(E) INTO refI
FROM Entreprise E
WHERE SIREN=1;

INSERT INTO Experience VALUES('eloi', TO_DATE('2005-01-01', 'YYYY-MM-DD'), TO_DATE('2005-12-31', 'YYYY-MM-DD'), refI, 'PDG', null);

SELECT REF(E) INTO refI
FROM Entreprise E
WHERE SIREN=2;

INSERT INTO Experience VALUES('eloi', TO_DATE('2006-01-01', 'YYYY-MM-DD'), TO_DATE('2006-12-31', 'YYYY-MM-DD'), refI, 'capo dei capi', null);

SELECT REF(E) INTO refI
FROM Entreprise E
WHERE SIREN=3;


INSERT INTO Experience VALUES('eloi', TO_DATE('2007-01-01', 'YYYY-MM-DD'), TO_DATE('2007-12-31', 'YYYY-MM-DD'), refI, 'conseiller de bill gates', null);

END;
/
SELECT exp.pseudo, exp.poste, exp.entreprise.nom
FROM Experience exp;
