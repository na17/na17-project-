-- Insertion de donnÃ©es
INSERT INTO Personne 
VALUES ('Numero1', 'numero1@email.com', 'Lucas', 'Dziurzik', TO_DATE('1974-05-02', 'YYYY-MM-DD'), null, null, 'MotDePasseTresSecurise1', TO_DATE('2012-05-02', 'YYYY-MM-DD'), TO_DATE('2019-01-01', 'YYYY-MM-DD'), 0, NULL, ADRESSE(4, 'rue de la paix','Paris' , 75008, 'France'));
INSERT INTO Personne 
VALUES ('CompiZoo', 'compizoo@email.com', 'Martin', 'Belair', TO_DATE('1989-07-02', 'YYYY-MM-DD'), null, null, 'MotDePasseTresSecurise5', TO_DATE('2014-05-19', 'YYYY-MM-DD'), TO_DATE('2019-01-07', 'YYYY-MM-DD'), 0, NULL, ADRESSE(25, 'avenue des pÃ¢queretes', 'Montsauche', 58495, 'France'));
INSERT INTO Personne 
VALUES ('Potiron', 'potiron@email.com', 'Louis', 'Potiron', TO_DATE('1995-04-02', 'YYYY-MM-DD'), null, null, 'MotDePasseIndevinable', TO_DATE('2014-12-20', 'YYYY-MM-DD'), TO_DATE('2019-04-04', 'YYYY-MM-DD'), 1, NULL, ADRESSE(17, 'ruelle des mendiants', 'Bolsolanoville', 25610, 'BrÃ©sil'));
INSERT INTO Personne 
VALUES ('La_Mouche', 'bzz@email.org', 'Maya', 'Dupontel', To_DATE('1998-05-31', 'YYYY-MM-DD'), null, null, 'BzzbzbzzZZbzzbZbz', TO_DATE('2016-09-22', 'YYYY-MM-DD'), TO_DATE('2019-04-17', 'YYYY-MM-DD'), 1, NULL, ADRESSE(1,'rue du professeur Lian', 'Treigny', 89350, 'France'));

INSERT INTO Personne
VALUES ('Personne_tres_competente','eloi-competence@gmail.com' , 'Eloi', NULL, NULL, NULL, NULL, 'motdepassedu10', TO_DATE('05-03-2004', 'DD-MM-YYYY'), NULL, 0, listCompetenceT(CompetenceT('Changer une barette de RAM','blablabla', 3), CompetenceT('Installer Linux','CrÃ©ation d''une clÃ© bootable et installation de linux n''importe quelle distribution', 4)), NULL);



-- test de la table Groupe
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux', 'Numero1', NULL, TO_DATE('2010-01-01', 'YYYY-MM-DD'), NULL);
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('JQuery', 'Le laisser mourrir en paix.', 'Potiron', NULL, TO_DATE('2017-01-31', 'YYYY-MM-DD'), NULL);

-- test de la table Adhesion
INSERT INTO Adhesion VALUES ('La_Mouche','JQuery',TO_DATE('2017-02-02', 'YYYY-MM-DD'),1);
INSERT INTO Adhesion VALUES ('Numero1','JQuery',TO_DATE('2018-04-02', 'YYYY-MM-DD'),1);
INSERT INTO Adhesion VALUES ('Numero1','Linux',TO_DATE('2018-04-02', 'YYYY-MM-DD'),1);
INSERT INTO Adhesion VALUES ('CompiZoo','Linux',TO_DATE('2018-04-02', 'YYYY-MM-DD'),1);


-- test de la table Administation
INSERT INTO Administration VALUES ('Linux', 'Numero1');
INSERT INTO Administration VALUES ('Linux', 'La_Mouche');


-- test de la table Bannissement
INSERT INTO Bannissement VALUES ('Numero1', 'Linux', 'La_Mouche', TO_DATE('2018-02-03', 'YYYY-MM-DD'), TO_DATE('2018-03-03', 'YYYY-MM-DD'));


-- test de la table Lien
INSERT INTO Lien (description) VALUES ('Collègue');
INSERT INTO Lien (description) VALUES ('Amis');
INSERT INTO Lien (description) VALUES ('Parents');
INSERT INTO Lien VALUES ('Cousins');
INSERT INTO Lien VALUES ('Fraternel');


-- test de la table Relation
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Numero1', 'CompiZoo', 'Amis');
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Potiron', 'CompiZoo', 'Collègue');
INSERT INTO Relation VALUES ('La_Mouche', 'Potiron', 'Cousins');


-- test de la table Formation
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('De linux Ã  l''autohÃ©bergement', 'DUT', 'Informatique', 25200, 'Linux, Docker, Nginx, MariaDB...');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('Puce microfluidique', 'Master 2', 'Biologie', 1314000, 'Introduction Ã  la crÃ©ation de puces microfluidiques spÃ©cialisÃ©es dans la culture cellulaire.');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('IHM et interculturalitÃ©', 'L1', 'Informatique', 50400, 'Comment traduire proprement une interface graphique ?');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('ThÃ©orie de la complexitÃ©', 'L2', 'Maths', 657000, 'Du calcul avec des examples de la vie de tout les jours.');
INSERT INTO Formation(nom, niveau, secteur, duree, description) VALUES ('IngÃ©nieur', 'Bac+5', 'Informatique', 5,'Ingenieur informatique Ã  l ecole UTC');
INSERT INTO Formation(nom, niveau, secteur, duree, description) VALUES ('Commerce', 'Bac+5',NULL, 3,'Etudes de commerce international');


-- test de la table SuiviDeFormation
INSERT INTO SuiviDeFormation VALUES ('Potiron', 'Puce microfluidique', TO_DATE('2015-02-23', 'YYYY-MM-DD'), NULL, 'Honorable', 'Je suis trÃ¨s Ã©mu.');
INSERT INTO SuiviDeFormation VALUES ('Numero1', 'IHM et interculturalitÃ©', NULL, 'IHM', 'Basique', NULL);




-- test de la table Entreprise
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (75285205, 'General Electrics', 'Electronique', 275600, 'USA', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 'Safran', 'AÃ©ronautique', 526490, 'France', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (056497465, 'Orange', 'telephonie', 85000, 'France', null);


-- test de la table Experience
INSERT INTO Experience VALUES ('La_Mouche', 75285205, TO_DATE('2014-01-01', 'YYYY-MM-DD'), TO_DATE('2017-01-01', 'YYYY-MM-DD'), 'IngÃ©nieur', 'ingÃ©nieur en Ã©lectronique');
INSERT INTO Experience VALUES ('Potiron', 56497465, TO_DATE('2011-09-01', 'YYYY-MM-DD'), TO_DATE('2013-04-30', 'YYYY-MM-DD'), 'Communication', 'Responsable de la communication');

-- test de la table Type
INSERT INTO Type VALUES ('article');
INSERT INTO Type VALUES ('commentaire');
INSERT INTO Type VALUES ('requete');
INSERT INTO Type VALUES ('news');
INSERT INTO Type VALUES ('URL');


-- test de la table Message
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (700, 'Numero1', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', 1, 0, 'article', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (702, 'Potiron', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', 1, 0, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (750, 'Numero1', TO_DATE('2014-03-10', 'YYYY-MM-DD'), 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', 1, 0, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (752, 'Numero1', TO_DATE('2014-03-10 11:25:10', 'YYYY-MM-DD HH:MI:SS'), 'JQuery', 'Introduction', 'Introduit les notions fondamentales ', 0 , 0, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'));


-- test de la table Aime
INSERT INTO Aime VALUES ('Potiron', 700);
INSERT INTO Aime VALUES ('La_Mouche', 700);
INSERT INTO Aime VALUES ('Potiron', 702);

-- test de la table Dossier
INSERT INTO Dossier VALUES ('Git', 'apprendre a utiliser git ainsi que github');







-- Fonctions de recherche basiques
        -- Rechercher une personne
SELECT pseudo, nom, prenom, date_naissance, lien_photo, date_inscription, email FROM Personne WHERE nom = 'Dziurzik' OR prenom = 'Lucas' OR pseudo = 'Numero1';
        -- Afficher son profil complet
SELECT Personne.pseudo, Personne.nom, Personne.prenom, Personne.date_naissance, Personne.lien_photo, Personne.date_inscription, Personne.email, SuiviDeFormation.nom, SuiviDeFormation.debut_suivi, SuiviDeFormation.specialite, SuiviDeFormation.mention
FROM Personne LEFT JOIN SuiviDeFormation ON Personne.pseudo = SuiviDeFormation.pseudo 
WHERE Personne.pseudo = 'La_Mouche';

-- Fonctions relatives aux groupes
SELECT COUNT(pseudo) FROM Adhesion WHERE nom='Linux'; -- Nombre de personne dans un groupe.
-- Rechercher un groupe, son nombre de membres et ses membres
SELECT nom, description, date_creation FROM Groupe WHERE nom = 'JQuery';
SELECT COUNT(*) FROM Adhesion WHERE nom = 'Linux';
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Adhesion ON Personne.pseudo = Adhesion.pseudo WHERE Adhesion.nom = 'Linux';
-- Afficher le nom, le prÃ©nom ainsi que la date de naissance des personnes dans le groupe Linux
SELECT P.nom, P.prenom, P.date_naissance FROM ADHESION A, Personne P WHERE A.nom = 'Linux' AND A.pseudo = P.pseudo GROUP BY P.nom, P.prenom, P.date_naissance;
-- Calculer le pseudo des personnes qui ont postÃ©s au moins 2 messages sur le groupe Linux 
SELECT M.auteur, count (*) FROM Message M WHERE M.groupe = 'Linux'  GROUP BY M.auteur  HAVING count(*) > 1;



-- Fonction relative aux messages
SELECT auteur, date_publication, groupe, titre, contenu FROM Message INNER JOIN Aime ON Message.numero = Aime.message WHERE Aime.pseudo = 'Potiron'; -- Liste des messages aimÃ©s par un utilisateur
-- Nombre de j'aimes d'un message
SELECT COUNT(*) FROM Aime WHERE message = 1;
-- Classement des message les plus likÃ©s
SELECT message, COUNT(*) FROM Aime GROUP BY message ORDER BY COUNT(*) DESC;
        -- Rechercher une entreprise 
SELECT * FROM Entreprise WHERE nom = 'General Electrics' OR siren = 75285205;
-- Afficher le nombre de messages aimÃ©s par toutes les personnes donc le prÃ©nom commence par la lettre 'L'
SELECT count(*) FROM Personne P, Aime A WHERE P.prenom LIKE 'L%' AND P.pseudo = A.pseudo; 






        -- Rechercher des personnes bannies jusqu'Ã  une certaine date 
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Bannissement ON Personne.pseudo = Bannissement.pseudo WHERE Bannissement.fin_bannissement = TO_DATE('03/03/2018', 'DD/MM/YYYY');
        -- Rechercher des personnes bannies depuis une certaine date 
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Bannissement ON Personne.pseudo = Bannissement.pseudo WHERE Bannissement.debut_bannissement = TO_DATE('03/02/2018', 'DD/MM/YYYY');




-- Afficher le nom des entreprises et leur secteur dans laquelle la personne avec le pseudo 'La_Mouche' a travaillÃ©
SELECT E.nom, E.secteur FROM Entreprise E, Experience Exp WHERE Exp.pseudo = 'La_Mouche' AND Exp.siren = E.siren;


-- Affichez la retlation entre 'Potiron' et 'Numero 1'
SELECT R.type_lien FROM  Relation R WHERE R.personne1 = 'Potiron'  AND R.personne2 = 'CompiZoo' ;
