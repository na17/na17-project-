CREATE TABLE Personne (
    pseudo VARCHAR PRIMARY KEY,
    email VARCHAR UNIQUE NOT NULL,
    prenom VARCHAR,
    nom VARCHAR,
    date_naissance DATE,
    lien_photo VARCHAR,
    telephone JSON, -- mis en JSON pour permettre une liste de numéros
    mot_de_passe VARCHAR NOT NULL,
    date_inscription DATE NOT NULL,
    date_derniere_connexion DATE,
    est_superadmin BOOLEAN NOT NULL,
    adresse JSON,
    Formation JSON,
    Competences JSON
);


CREATE TABLE Groupe (
    nom VARCHAR PRIMARY KEY,
    description TEXT,
    createur VARCHAR REFERENCES Personne(pseudo) NOT NULL,
    parent VARCHAR REFERENCES Groupe(nom),
    date_creation DATE NOT NULL,
    suppression VARCHAR REFERENCES Personne(pseudo)
);


CREATE TABLE Adhesion (
    pseudo VARCHAR REFERENCES Personne(pseudo),
    nom VARCHAR REFERENCES Groupe(nom),
    date_demande DATE NOT NULL,
    adhesion_validee BOOLEAN NOT NULL,
    PRIMARY KEY(pseudo, nom)
);


CREATE TABLE Administration (
    nom VARCHAR REFERENCES Groupe(nom),
    pseudo VARCHAR REFERENCES Personne(pseudo),
    PRIMARY KEY(pseudo, nom)
);


CREATE TABLE Bannissement (
    pseudo VARCHAR REFERENCES Personne(pseudo),
    nom VARCHAR REFERENCES Groupe(nom),
    banni_par VARCHAR REFERENCES Personne(pseudo) NOT NULL,
    debut_bannissement DATE NOT NULL,
    fin_bannissement DATE,
    PRIMARY KEY(pseudo, nom)
);


CREATE TABLE Lien (
    description VARCHAR PRIMARY KEY
);

CREATE TABLE Relation (
    personne1 VARCHAR REFERENCES Personne(pseudo),
    personne2 VARCHAR REFERENCES Personne(pseudo),
    type_lien VARCHAR REFERENCES Lien(description),
    PRIMARY KEY (personne1, personne2)
);




CREATE TABLE Entreprise (
    siren INTEGER PRIMARY KEY,
    nom VARCHAR NOT NULL,
    secteur VARCHAR,
    nb_employes INTEGER,
    pays VARCHAR,
    logo_url VARCHAR
);


CREATE TABLE Experience (
    pseudo VARCHAR REFERENCES Personne(pseudo),
    siren INTEGER REFERENCES Entreprise(siren),
    debut DATE NOT NULL,
    fin DATE,
    poste VARCHAR NOT NULL,
    description TEXT,
    PRIMARY KEY (pseudo, siren, debut)
);


CREATE TABLE Type (
    nom VARCHAR PRIMARY KEY
);


CREATE TABLE Message (
	numero INTEGER PRIMARY KEY,
    auteur VARCHAR REFERENCES Personne(pseudo) NOT NULL,
    date_publication TIMESTAMP NOT NULL,
    groupe VARCHAR REFERENCES Groupe(nom) NOT NULL,
    titre_et_contenu JSON NOT NULL,
    est_commentable BOOLEAN NOT NULL,
    supprime BOOLEAN NOT NULL,
    type VARCHAR REFERENCES Type(nom) NOT NULL,
    parent INTEGER REFERENCES Message(numero),
    date_derniere_modification DATE,
	tags JSON,
	UNIQUE(auteur, date_publication)
);


CREATE TABLE Aime (
    pseudo VARCHAR REFERENCES Personne(pseudo),
    message INTEGER REFERENCES Message(numero),  
    PRIMARY KEY(pseudo, message)
);


CREATE TABLE Dossier (
    nom VARCHAR PRIMARY KEY,
    description TEXT
);
