-- Insertion de données dans la base

-- Insertion de données
INSERT INTO Personne 
VALUES ('Numero1', 'numero1@email.com', 'Lucas', 'Dziurzik', TO_DATE('1974-05-02', 'YYYY-MM-DD'), null, '[{"type":"fixe", "numero":"03.44.94.28.40"},{"type":"bureau", "numero":"01.23.45.67.89"}]', 'MotDePasseTresSecurise1', TO_DATE('2012-05-02', 'YYYY-MM-DD'), TO_DATE('2019-01-01', 'YYYY-MM-DD'), FALSE, '{"numero":4, "rue":"rue de la paix","ville":"Paris", "CP": "75008", "pays":"France"}', '[{"formation":"IHM et interculturalité", "specialite":"IHM", "mention": "Basique","niveau":"L1", "domaine":"Informatique", "duree":"6 mois", "description":"Comment traduire proprement une interface graphique ?"}]', '[]');
INSERT INTO Personne 
VALUES ('CompiZoo', 'compizoo@email.com', 'Martin', 'Belair', TO_DATE('1989-07-02', 'YYYY-MM-DD'), null, '[{"type":"ferme", "numero":"04.42.50.14.32"}]', 'MotDePasseTresSecurise5', TO_DATE('2014-05-19', 'YYYY-MM-DD'), TO_DATE('2019-01-07', 'YYYY-MM-DD'), FALSE, '{"numero":25, "rue":"avenue des pâquerettes", "ville":"Montsauche", "CP": "58495", "pays": "France"}', '[]', '[]');
INSERT INTO Personne 
VALUES ('Potiron', 'potiron@email.com', 'Louis', 'Potiron', TO_DATE('1995-04-02', 'YYYY-MM-DD'), null, '[]', 'MotDePasseIndevinable', TO_DATE('2014-12-20', 'YYYY-MM-DD'), TO_DATE('2019-04-04', 'YYYY-MM-DD'), TRUE, '{"numero":17, "rue":"ruelle des mendiants", "ville":"Bolsolanoville", "CP": "25610", "pays": "Brésil"}', '[{"formation":"Puce microfluidique", "date":"2015-02-23", "mention": "Honorable", "commentaire":"Je suis très ému.", "niveau":"Master 2", "domaine":"Biologie", "duree":"1 an", "description":"Introduction à la création de puces microfluidiques spécialisées dans la culture cellulaire."}]', '[]');
INSERT INTO Personne 
VALUES ('La_Mouche', 'bzz@email.org', 'Maya', 'Dupontel', To_DATE('1998-05-31', 'YYYY-MM-DD'), null, '[]', 'BzzbzbzzZZbzzbZbz', TO_DATE('2016-09-22', 'YYYY-MM-DD'), TO_DATE('2019-04-17', 'YYYY-MM-DD'), TRUE, '{"numero":1, "rue":"rue du professeur Lian", "ville":"Treigny", "CP": "89350", "pays": "France"}', '[{"formation":"IHM et interculturalité", "specialite":"interculturalité", "mention": "reçut","niveau":"L1", "domaine":"Informatique", "duree":"6 mois", "description":"Comment traduire proprement une interface graphique ?"}]', '[]');

INSERT INTO Personne
VALUES ('Personne_tres_competente','eloi-competence@gmail.com' , 'Eloi', NULL, NULL, NULL, '[]', 'motdepassedu10', TO_DATE('05-03-2004', 'DD-MM-YYYY'), TO_DATE('05-03-2004', 'DD-MM-YYYY'), FALSE, '{}', '[{"formation":"Nettoyer un appareil éléctronique", "specialite":"Minitel", "mention": "Admirable","niveau":"Doctorat", "domaine":"Informatique", "duree":"3 ans", "description":"Théorie du nettoyage de circuits imprimés en tout genre"}]', '[{"competence":"Changer une barette de RAM","description":"blablabla", "expertise":3}, {"competence":"Installer Linux","description":"Création d''une clé bootable et installation de linux n''importe quelle distribution", "expertise":4}]');


-- test de la table Groupe
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux', 'Numero1', NULL, TO_DATE('2010-01-01', 'YYYY-MM-DD'), NULL);
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('JQuery', 'Le laisser mourrir en paix.', 'Potiron', NULL, TO_DATE('2017-01-31', 'YYYY-MM-DD'), NULL);

-- test de la table Adhesion
INSERT INTO Adhesion VALUES ('La_Mouche','JQuery',TO_DATE('2017-02-02', 'YYYY-MM-DD'), TRUE);
INSERT INTO Adhesion VALUES ('Numero1','JQuery',TO_DATE('2018-04-02', 'YYYY-MM-DD'), TRUE);
INSERT INTO Adhesion VALUES ('Numero1','Linux',TO_DATE('2018-04-02', 'YYYY-MM-DD'), TRUE);
INSERT INTO Adhesion VALUES ('CompiZoo','Linux',TO_DATE('2018-04-02', 'YYYY-MM-DD'), TRUE);


-- test de la table Administation
INSERT INTO Administration VALUES ('Linux', 'Numero1');
INSERT INTO Administration VALUES ('Linux', 'La_Mouche');


-- test de la table Bannissement
INSERT INTO Bannissement VALUES ('Numero1', 'Linux', 'La_Mouche', TO_DATE('2018-02-03', 'YYYY-MM-DD'), TO_DATE('2018-03-03', 'YYYY-MM-DD'));


-- test de la table Lien
INSERT INTO Lien (description) VALUES ('Collègue');
INSERT INTO Lien (description) VALUES ('Amis');
INSERT INTO Lien (description) VALUES ('Parents');
INSERT INTO Lien VALUES ('Cousins');
INSERT INTO Lien VALUES ('Fraternel');


-- test de la table Relation
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Numero1', 'CompiZoo', 'Amis');
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Potiron', 'CompiZoo', 'Collègue');
INSERT INTO Relation VALUES ('La_Mouche', 'Potiron', 'Cousins');


-- test de la table Entreprise
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (75285205, 'General Electrics', 'Electronique', 275600, 'USA', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 'Safran', 'Aéronautique', 526490, 'France', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (056497465, 'Orange', 'telephonie', 85000, 'France', null);


-- test de la table Experience
INSERT INTO Experience VALUES ('La_Mouche', 75285205, TO_DATE('2014-01-01', 'YYYY-MM-DD'), TO_DATE('2017-01-01', 'YYYY-MM-DD'), 'Ingénieur', 'ingénieur en électronique');
INSERT INTO Experience VALUES ('Potiron', 56497465, TO_DATE('2011-09-01', 'YYYY-MM-DD'), TO_DATE('2013-04-30', 'YYYY-MM-DD'), 'Communication', 'Responsable de la communication');

-- test de la table Type
INSERT INTO Type VALUES ('article');
INSERT INTO Type VALUES ('commentaire');
INSERT INTO Type VALUES ('requete');
INSERT INTO Type VALUES ('news');
INSERT INTO Type VALUES ('URL');


-- test de la table Message
INSERT INTO Message  VALUES (700, 'Numero1', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux', '{"titre":"installer linux","contenu":"explique comment installer linux sur son ordinateur"}', TRUE, FALSE, 'article', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'), '["Pinguin"]');
INSERT INTO Message  VALUES (702, 'Potiron', TO_DATE('2014-03-09', 'YYYY-MM-DD'), 'Linux', '{"titre":"installer linux","contenu":"Il suffit de télécharger ubuntu sur le site officiel."}', TRUE, FALSE, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'), '["Distro"]');
INSERT INTO Message  VALUES (750, 'Numero1',  TO_DATE('2014-03-10 11:25:10', 'YYYY-MM-DD HH:MI:SS'), 'Linux', '{"titre":"installer linux","contenu":"Mais tu peux aussi télécharger une autre distribution sur GitLab !"}', TRUE, FALSE, 'commentaire', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'), '["Git"]');
INSERT INTO Message VALUES (752, 'Numero1', TO_DATE('2014-03-12', 'YYYY-MM-DD'), 'JQuery', '{"titre":"Introduction","contenu":"Introduit les notions fondamentales"}', FALSE , FALSE, 'article', NULL, TO_DATE('2016-08-12', 'YYYY-MM-DD'), '["Js","Async","Web"]');


-- test de la table Aime
INSERT INTO Aime VALUES ('Potiron', 700);
INSERT INTO Aime VALUES ('La_Mouche', 700);
INSERT INTO Aime VALUES ('Potiron', 702);

-- test de la table Dossier
INSERT INTO Dossier VALUES ('Git', 'Apprendre a utiliser git ainsi que gitlab.');
INSERT INTO Dossier VALUES ('Distro', 'Choisir une distribution linux.');
INSERT INTO Dossier VALUES ('Js', 'Tout sur javascript.');
INSERT INTO Dossier VALUES ('Async', 'Programmation asynchrone.');
INSERT INTO Dossier VALUES ('Web', 'Programmation web.');