-- Affiche les numéros de téléphone de tout les utilisateurs.
SELECT p.pseudo, phone.* FROM Personne p, JSON_TO_RECORDSET(p.telephone) AS phone (numero VARCHAR, type VARCHAR);

-- Retourne quelques informations relative à l'adresse des utiliateur.
SELECT p.pseudo, p.adresse->>'pays' AS Pays, p.adresse->>'rue' AS Rue, p.adresse->>'ville' AS Ville  FROM Personne p;

-- Affiche les messages et leur nombre de tag 
SELECT m.titre_et_contenu->>'titre' AS titre, m.titre_et_contenu->>'contenu' AS message, m.auteur, COUNT(d.*) AS tag_number FROM Message m LEFT JOIN JSON_ARRAY_ELEMENTS(m.tags) AS d ON TRUE GROUP BY m.numero;

-- Même requete (plus performante) en utilisant json_array_length
SELECT m.titre_et_contenu->>'titre' AS titre, m.titre_et_contenu->>'contenu' AS message, m.auteur, json_array_length(m.tags) AS tag_number FROM Message m; 

-- Affiche les couples (message, tag) accompagné (si elle existe) de la description du tag.
SELECT m.numero, t.value AS tag, d.description FROM Message m, JSON_ARRAY_ELEMENTS_TEXT(m.tags) AS t LEFT JOIN Dossier d ON t.value=d.nom; 

-- Selectionne les pseudo des personnes ayant suivies une formation en informatique en donnant quelques informations sur la dite formation

SELECT p.pseudo, formation.formation, formation.specialite , formation.mention FROM Personne p, JSON_TO_RECORDSET(p.Formation) AS formation (domaine VARCHAR, formation VARCHAR, specialite VARCHAR, mention VARCHAR) WHERE formation.domaine='Informatique';

-- Selectionne les pseudo des personnes ayant une expertise supérieur à 2 dans une compétence.

SELECT p.pseudo, comp.* FROM Personne p, JSON_TO_RECORDSET(p.Competences) AS comp (competence VARCHAR, description TEXT, expertise INTEGER) WHERE comp.expertise>2;
