echo "Insertion des données json dans la base na17:"
mongoimport --jsonArray --db na17 --collection Formation --file Formation.json
mongoimport --jsonArray --db na17 --collection Entreprise --file Entreprise.json
mongoimport --jsonArray --db na17 --collection Dossier --file Dossier.json
mongoimport --jsonArray --db na17 --collection Personne --file Personne.json
mongoimport --jsonArray --db na17 --collection Groupe --file Groupe.json