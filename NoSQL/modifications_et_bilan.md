# Modifications UML :


Les principes du réseau social sont restés les mêmes. Cependant les éléments modifiés sont les suivants :


	- Compositions :

		Personne avec : 
			SuiviDeFormation, Experience, Compétence.


		Groupe avec :
			Adhésion, Bannissement, Messages. (Les messages doivent donc maintenant impérativement appartenir à un groupe).




	- Suppression de la relation entre message et type. Nous avons fait passer le type comme un attribut de la classe message :
		Nous avons pensé à l'implémentation, il y a peu d'intérêt à avoir 2 collections distinctes pour les messages et les types de messages.
		Cela aurait impliqué des opérations de jointures inutiles, de plus la redondance est faible.


# Bilan :

### Avantages :

	Pour certaines requêtes, on gagne en efficacité (exemple : accès direct aux messages depuis un groupe, avec moins d'opérations de jointures).
	Intéressant pour des statistiques sur des gros volumes de données.

### Inconvénients :

	Cependant, il y a redondance de données.
	De plus, l'utilisation de l'id est assez problématique, notamment pour la création de références entre différentes collections.