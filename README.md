# Réseau social de veille interactive


## Rappel du sujet

>  L'objectif du projet est de réaliser un réseau social de veille technologique qui comprend les fonctions suivantes :
>  *  Créer des personnes
>  *  Associer des informations à ces personnes : lieux de travail, entreprise dans lesquelles elles travaillent, formations suivies, diplômes obtenus, compétences techniques...
>  * Créer des liens typés entre les personnes (ami, collègue, connaissance...)
>  * Créer des groupes et sous-groupes thématiques de veille (Linux, Postgres...) et permettre aux personnes d'adhérer à ces groupes
>  * Permettre aux personnes de poster des messages typés (news, articles, URL, document, photographie...) et éventuellement de les associer à des groupes ou sous-groupes
>  * Créer des dossiers thématiques permettant de regrouper des posts

## Membres du groupe 
- Eloi JUSZCZAK
- Mathilde LE MOEL
- Candice LEGEAY
- Louis PINEAU
- Aymerik DIEBOLD

## Livrables

| Livrable | Status |
| ------ | ------ |
| __NDC__  | __✔ Faite__ |
| __MCD__ | __✔ Fait__ |
| __NDR__ | __✔ Faite__ |
| __MLD__ | __✔ Fait__  |
| __SQL LDD__ | __✔ Fait__ |
| __SQL LMD__ | __✔ Fait__ |
| POC MongoDB | ✗ À faire |
| POC Oracle/XML | ✗ À faire |
| POC PostgreSQL/JSON | ✗ À faire |

Le Modèle Conceptuel de Données est disponible au format SVG (Relationnel/img/MCD.svg) et au format source puml (Relationnel/ModèleConceptuelDeDonnees.puml).
## Relationnel
### Fonctionnalités
Les fonctionnalités sont disponibles dans le fichier ```LMD.sql```.
- Fonctions de recherche basiques
- 

## NoSQL
Pour insérer les données dans la base mongodb, il suffit d'executer le fichier ```INSERT.bash``` situé dans le dossier ```NoSQL/collections/```.