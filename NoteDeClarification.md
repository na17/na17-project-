# Note de clarification

## Vision du problème

Le réseau social de veille technologique est un outil servant à la fois à se renseigner et maintenir ses connaissances technologiques dans un ou plusieurs domaines, et à la fois permettant de regrouper des membres par leurs domaines de connaissances, leurs intérêts, leurs métiers…

Dans ce réseau social, chaque personne est associée à un compte. Chaque utilisateur peut créer un groupe, rejoindre ou non un groupe, demander à créer un sous-groupe à l’administrateur du groupe, lire les documents relatifs à un thème, commenter certains posts (selon ses droits).

Les groupes servent à rassembler leurs membres autour d’un même thème qui sera symbolisé par le titre du groupe. Les groupes sont gérés par un ou plusieurs administrateurs, qui peuvent accepter et bannir des membres, supprimer des messages. Chaque membre du groupe peut poster dans le groupe et commenter les autres posts du groupe. Les utilisateurs étrangers au groupe ne peuvent pas commenter les posts du groupe mais peuvent poster des requêtes.

Les dossiers thématiques servent à rassembler les publications liées à un thème, qu’elles aient été publiées dans un groupe ou non. Tous les utilisateurs peuvent avoir accès à tous les dossiers thématiques sans devoir nécessairement être membre d’un groupe, car le but d’un réseau de veille technologique est de pouvoir se renseigner. Les publications sont typées et sont identifiées par un ou plusieurs tags qui permettent le regroupement thématique. 

Les messages postés dans un groupe deviennent donc une source pour un dossier thématique, mais n’en sont pas l’unique source car il est possible de poster en dehors d’un groupe un article (ou autre) sur un thème.



__Personnes__ :
  * pseudo unique
  * email unique
  * nom, prénom, date de naissance (optionnels)
  * photo (optionnelle)
  * mot de passe
  * possibilité de liens avec d’autres personnes (amis, famille, collègues)
  * peut adhérer à un ou plusieurs groupes
  * peut poster un ou plusieurs messages
  * peut créer un groupe : en devient l’administrateur
  * peut être un administrateur
  * peut avoir travaillé pour certaines entreprises, pendant une certaine période et pour une durée précise
  * peut avoir plusieurs compétences, à un niveau différent
  * peut avoir suivi plusieurs formations pendant une certaine période, avec une éventuelle spécialité et une éventuelle mention
  * peut lire tous les posts
  * peut commenter les posts “libres” ou des groupes auxquels il appartient
  * peut être un super-administrateur (et agir ainsi sur tous les groupes et messages)
  * peut aimer des messages


__Groupe__: 
  * nom unique
  * administré par une ou plusieurs personnes
  * peut avoir un ou plusieurs sous-groupes
  * peut être le sous-groupe d’un groupe 
  * contient aucune, une ou plusieurs personnes
  * indique les informations du groupe (nombre de membres, nombre de sous-groupes)

__Administrateur__ :
  * est une personne
  * dirige un ou plusieurs groupes
  * peut valider l’adhésion d'une personne dans un groupe
  * peut bannir une personne du groupe à partir d'une certaine date et pour une durée définie
  * peut supprimer des messages postés dans le groupe

__Dossiers thématiques__ :
  * titre unique
  * contient un ou plusieurs posts
  * regroupe les posts qui ont le même tag
  * est créé lorsque le premier message relatif à ce thème (identifié par le tag) est posté
  * est accessible par tous les utilisateurs

__Message typé/Post__ :
  * titre optionnel
  * est posté par une personne 
  * est typé (requête, news, article, URL, document, photographie) : type prédéfini 
  * peut être un commentaire d’un message 
  * est identifié par une date et une heure
  * peut être relatif à un groupe (ce post pourra être commenté seulement par les personnes appartenant au groupe)
  * possède un ou plusieurs tags
  * appartient à un dossier thématique
  * peut être commenté (par tous pour les messages typés “libres” ou seulement par les membres du groupe s’il est typé commentaire d’un post d’un groupe)
  * peut être masqué (sans pour autant être définitivement supprimé des données)
  * peut être aimé par des utilisateurs (like)

__Formation__ :
  * peut être suivie par plusieurs personnes
  * est identifiée par un nom
  * peut avoir une certaine durée (en mois)
  * peut avoir un niveau (exemple : Bac + 5) et un secteur (exemple : Développement informatique)

__Entreprise__ :
  * peut avoir plusieurs employés
  * est caractérisée par son numéro de SIREN
  * comporte un NIC (permettant ainsi d'obtenir le SIRET)
  * s'inscrit dans un secteur d'activité