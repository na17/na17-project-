Personne(#pseudo : String, email : String, nom : String, prenom : String, date_naissance : Date, lien_photo : String, mot_de_passe : String, numero : String, date_inscription : Date, date_derniere_connexion : Date, est_superadmin : booleen) avec email KEY, mot_de_passe, date_inscription NOT NULL

Groupe(#nom : String, description : String, createur => Personne.pseudo, parent => Groupe.nom, date_creation : Date, suppression => Personne.pseudo) avec createur, date_creation NOT NULL

Adhesion(#pseudo => Personne, #nom => Groupe, adhesion_validee : booléen, date_demande : Date) date_demande, adhesion_validee NOT NULL

Administration(#nom => Groupe, #pseudo => Personne)

Bannissement(#nom => Groupe, #pseudo => Personne, banni_par => Personne.pseudo, début_bannissement : Date, fin_bannissement : Date) avec banni_par, debut_banissement NOT NULL

Lien(#description: String)

Relation(#personne1 => Personne.pseudo, #personne2 => Personne.pseudo, type_lien => Lien.description) type_lien NOT NULL

Formation(#nom : String, niveau : String, secteur : String, duree : int, description : String)

SuiviDeFormation(#pseudo => Personne, #nom => Formation, début_suivi : Date, spécialité : String, mention : String, commentaire : String)

Compétence(#nom : String, description : String)

Expertise(#pseudo => Personne, #nom => Compétence, niveau : int, commentaire : String) avec niveau compris entre 0 et 5 et NOT NULL.

Entreprise(#siren : int, nom : String, secteur : String, nb_employes : int, pays : String, logo_url : String) avec nom NOT NULL.

Experience(#pseudo => Personne, #siren => Entreprise, #début : Date, fin : Date, poste : String, description : String) poste NOT NULL

Type(#nom : String)

Message(#numero : int, auteur => Personne.pseudo, date_publication : DateTime, groupe => Groupe.nom, titre : String, contenu : String, est_commentable : booléen, supprimé : booléen, type => Type.nom, parent => Message.numero, date_derniere_modification : DateTime) avec groupe, type, contenu, est_commentable, supprimé NOT NULL & (auteur, date_publication) clé candidate

Aime(#pseudo => Personne, #message => Message.numero)

Dossier(#nom : String, description : String)

Tags(#message => Message.numero, #nom => Dossier.nom)

