-- Données de test

-- test de la table Personne
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('Numero1', 'numero1@email.com', 'Lucas', 'Dziurzik', '1974-05-02', null, null, 'MotDePasseTresSecurise1', '2012-05-02', '2019-01-01', False);
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('CompiZoo', 'compizoo@email.com', 'Martin', 'Belair', '1989-07-02', null, null, 'MotDePasseTresSecurise5', '2014-05-19', '2019-01-07', False);
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('Potiron', 'potiron@email.com', 'Louis', 'Potiron', '1995-04-02', null, null, 'MotDePasseIndevinable', '2014-12-20', '2019-04-04', True);
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('La_Mouche', 'bzz@email.org', 'Maya', 'Dupontel', '1998-05-31', null, null, 'BzzbzbzzZZbzzbZbz', '2016-09-22', '2019-04-17', True);

-- test de la table Groupe
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux', 'Numero1', NULL, '2010-01-01', NULL);
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('JQuery', 'Le laisser mourrir en paix.', 'Potiron', NULL, '2017-01-31', NULL);

-- test de la table Adhesion
INSERT INTO Adhesion VALUES ('La_Mouche','JQuery','2017-02-02',True);
INSERT INTO Adhesion VALUES ('Numero1','JQuery','2018-04-02',True);
INSERT INTO Adhesion VALUES ('Numero1','Linux','2018-04-02',True);
INSERT INTO Adhesion VALUES ('CompiZoo','Linux','2018-04-02',True);

-- test de la table Administation
INSERT INTO Administration VALUES ('Linux', 'Numero1');
INSERT INTO Administration VALUES ('Linux', 'La_Mouche');

-- test de la table Bannissement
INSERT INTO Bannissement VALUES ('Numero1', 'Linux', 'La_Mouche', '2018-02-03', '2018-03-03');

-- test de la table Lien
INSERT INTO Lien (description) VALUES ('Collègue');
INSERT INTO Lien (description) VALUES ('Amis');
INSERT INTO Lien (description) VALUES ('Parents');
INSERT INTO Lien VALUES ('Cousins');
INSERT INTO Lien VALUES ('Fraternel');

-- test de la table Relation
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Numero1', 'CompiZoo', 'Amis');
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Potiron', 'CompiZoo', 'Collègue');
INSERT INTO Relation VALUES ('La_Mouche', 'Potiron', 'Cousins');

-- test de la table Formation
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('De linux à l''autohébergement', 'DUT', 'Informatique', 25200, 'Linux, Docker, Nginx, MariaDB...');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('Puce microfluidique', 'Master 2', 'Biologie', 1314000, 'Introduction à la création de puces microfluidiques spécialisées dans la culture cellulaire.');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('IHM et interculturalité', 'L1', 'Informatique', 50400, 'Comment traduire proprement une interface graphique ?');
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('Théorie de la complexité', 'L2', 'Maths', 657000, 'Du calcul avec des examples de la vie de tout les jours.');
INSERT INTO Formation(nom, niveau, secteur, duree, description) VALUES ('Ingénieur', 'Bac+5', 'Informatique', 5,'Ingenieur informatique à l ecole UTC');
INSERT INTO Formation(nom, niveau, secteur, duree, description) VALUES ('Commerce', 'Bac+5',NULL, 3,'Etudes de commerce international');

-- test de la table SuiviDeFormation
INSERT INTO SuiviDeFormation VALUES ('Potiron', 'Puce microfluidique', '2015-02-23', NULL, 'Honorable', 'Je suis très ému.');
INSERT INTO SuiviDeFormation VALUES ('Numero1', 'IHM et interculturalité', NULL, 'IHM', 'Basique', NULL);

-- ajout de données pour la table Compétence
INSERT INTO Competence (nom, description) VALUES ('Changer une barette de RAM','blablabla');
INSERT INTO Competence (nom, description) VALUES ('Installer Linux','Création d''une clé bootable et installation de linux n''importe quelle distribution');

-- test de la table Expertise
INSERT INTO Expertise (pseudo, nom, niveau, commentaire) VALUES ('Numero1', 'Installer Linux', 2, 'J''ai déjà installé 2 dual boot mais ça s''est transformé en mono boot, adieu Windows!');
INSERT INTO Expertise (pseudo, nom, niveau, commentaire) VALUES ('Potiron', 'Changer une barette de RAM', 4, 'Je suis le roi du tournevis');

-- test de la table Entreprise
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (75285205, 'General Electrics', 'Electronique', 275600, 'USA', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 'Safran', 'Aéronautique', 526490, 'France', null);
INSERT INTO Entreprise (siren, nom, secteur, nb_employes, pays, logo_url) VALUES (056497465, 'Orange', 'telephonie', 85000, 'France', null);

-- test de la table Experience
INSERT INTO Experience VALUES ('La_Mouche', 75285205, '2014-01-01', '2017-01-01', 'Ingénieur', 'ingénieur en électronique');
INSERT INTO Experience VALUES ('Potiron', 56497465, '2011-09-01', '2013-04-30', 'Communication', 'Responsable de la communication');

-- test de la table Type
INSERT INTO Type Values ('article');
INSERT INTO Type Values ('commentaire');
INSERT INTO Type VALUES ('requete');
INSERT INTO Type VALUES ('news');
INSERT INTO Type VALUES ('URL');


-- test de la table Message
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (700, 'Numero1', '2014-03-09', 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', True, False, 'article', NULL, '2016-08-12');
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (702, 'Potiron', '2014-03-09', 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', True, False, 'commentaire', NULL, '2016-08-12');
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (750, 'Numero1', '2014-03-10', 'Linux', 'installer linux', 'explique comment installer linux sur son ordinateur', True, False, 'commentaire', NULL, '2016-08-12');
INSERT INTO Message (numero, auteur, date_publication, groupe, titre, contenu, est_commentable, supprime, type, parent, date_derniere_modification) VALUES (752, 'Numero1', '2014-03-10 14:25:10', 'JQuery', 'Introduction', 'Introduit les notions fondamentales ', False , False, 'commentaire', NULL, '2016-08-12');


-- test de la table Aime
INSERT INTO Aime Values ('Potiron', 700);
INSERT INTO Aime Values ('La_Mouche', 700);
INSERT INTO Aime Values ('Potiron', 702);

-- test de la table Dossier
INSERT INTO Dossier Values ('Git', 'apprendre a utiliser git ainsi que github');

-- test de la table Tags
INSERT INTO Tags VALUES ('Git', 700);