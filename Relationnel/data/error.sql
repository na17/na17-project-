-- Données ne devant pas être acceptée dans la base après l'insertion des données de test.

-- Formation
INSERT INTO Formation (nom, niveau, secteur, duree, description) VALUES ('De linux à l''autohébergement', 'Master 26', 'Psycho-informatique', 125200, 'Magie et sorcèlerie sur ordinateur.'); -- Test de la contrainte d'unicité de la clé primaire

-- Personne
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('Numero1', 'numero1@email.com', 'Lucas', 'Dziurzik', '1974-05-02', null, null, 'MotDePasseTresSecurise2', '2012-05-02', '2019-01-01', False); -- Test de la contrainte sur la clé primaire
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('Numero2', 'numero1@email.com', 'Lucas', 'Dziurzik', '1974-05-02', null, null, 'MotDePasseTresSecurise4', '2012-05-02', '2019-01-01', False); -- test de la contrainte email unique
INSERT INTO Personne (pseudo, email, prenom, nom, date_naissance, lien_photo, numero, mot_de_passe, date_inscription, date_derniere_connexion, est_superadmin) VALUES ('Numero2', null , 'Lucas', 'Dziurzik', '1974-05-02', null, null, 'MotDePasseTresSecurise4', '2012-05-02', '2019-01-01', False); -- Test de la contrainte email null

-- Relation
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Numero2', 'CompiZoo', 'Amis'); --test de la contrainte de référence sur la table Personne
INSERT INTO Relation (personne1, personne2, type_lien) VALUES ('Potiron', 'CompiZoo', 'pto'); --test de la contrainte de référence sur la table Lien

-- Expertise
INSERT INTO Expertise (pseudo, nom, niveau, commentaire) VALUES ('Numero1', 'Mettre en place son propre serveur', 4, 'J''ai été Webmaster du site quelbeauchien.fr pendant 5ans'); --test sur la contrainte référence à Compétence
INSERT INTO Expertise (pseudo, nom, niveau, commentaire) VALUES ('Numero2', 'Installer Linux', 2, 'J''ai déjà installé 2 dual boot mais ça s''est transformé en mono boot, adieu Windows!'); --test sur la contrainte référence à Personne

-- Entreprise
INSERT INTO Entreprise (siren, nic, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 12654120, 'Safran', 'Aéronautique', 526490, 'France', null); --test de la clé primaire
INSERT INTO Entreprise (siren, nic, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 12654120, 'General Electrics', 'Electronique', 275600, 'USA', null);
INSERT INTO Entreprise (siren, nic, nom, secteur, nb_employes, pays, logo_url) VALUES (12301252, 12654120, null, 'Aéronautique', 526490, 'France', null); --test du nom NOT NULL
INSERT INTO Entreprise (siren, nic, nom, secteur, nb_employes, pays, logo_url) VALUES (12322520, null, 'Safran', 'Aéronautique', 526490, 'France', null); --test du nic NOT NULL

-- Groupe
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Windows', 'tout sur windows', 'Eloi', NULL, '01-01-2011', NULL); --test avec un createur qui n'existe pas
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('Linux', 'tout sur linux 2', 'Numero1', NULL, '01-01-2011', NULL); --test avec 2 groupes de même nom
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('C++', 'tout sur le C++', 'Numero1', 'Langage de programmation', '01-01-2011', NULL); --test d'un sous groupe d'un groupe qui n'existe pas
INSERT INTO Groupe (nom, description, createur, parent, date_creation, suppression) VALUES ('C++', 'tout sur le C++', 'Numero1', NULL, '01-01-2011', 'Eloi'); --test d'un sous groupe d'un groupe qui n'existe pas