# Note de rectification
_Quelques modifications ont été apportées depuis la semaine précédente au cours de la réalisation du MCD. Voici un détail des plus importantes modifications apportées_ 

## Utilisateur
* Un utilisateur peut être **super-administrateur**, afin de pouvoir administrer l'ensemble des groupes (et supprimer un groupe qui ne respecte pas le règlement du réseau social par exemple.
* Un utilisateur peut renseigner ses **formations** : quelles formations a-t-il suivi, à quelle période, avec quelle éventuelle spécialité et mention ?
* Un utilisateur peut renseigner ses **emplois** : dans quelles entreprises a-t-il travaillé, à quelle période et sous quel poste ?
* Un utilisateur peut renseigner ses **compétences** et le niveau associé
* Un utilisateur peut maintenant **aimer des messages** *(likes)*

## Messages

Concernant les messages, nous avons ajouté un attribut : le booléen *supprimé* qui indique si le message a été modéré, auquel cas, il restera dans la base donnée mais ne sera plus visible par les utilisateurs.

## Bannissements
Concernant les bannissements, nous avons trouvé pertinent d'ajouter une date de début et de fin à un ban. De plus, nous avons décidé d'informer par qui était bannie une personne.

## MLD & LDD

Ajout contrainte date_inscription NOT NULL dans la relation Personne, (derniere connexion peut être null : cas où qqun s'est inscrit mais jamais encore connecté)
Ajout contrainte date_creation NOT NULL dans la relation groupe.
Ajout contrainte date_demande, adhesion_validee NOT NULL dans la relation adhesion.
Ajout contrainte debut_banissement NOT NULL dans la relation Banissement (fin_banissement peut être null <=> banissement à vie)
Ajout contrainte type_lien NOT NULL dans la relation Relation.
Ajout contrainte niveau NOT et compris entre 0 et 5, dans la relation Expertise.
Passage de début en clé de la relation Experience (une personne peut avoir été embauché 2 fois par une même entreprise, au même poste à 2 dates différentes. Exemple : embauché le 01/01/1990 chez Apple en tant qu'ingénieur, licencié le 02/01/1990, puis réembauché au même poste le 03/01/1990)
Ajout contrainte est_commentable, supprimé NOT NULL dans la relation Message.

ajout de la clé candidate(auteur, date_publication) dans la relation Message. (une personne ne peut pas poster 2 messages exactement au même moment).
On n'enlève pas la clé artificielle, afin que les clés étrangères référançants la relation Message soient plus facilement utilisables.