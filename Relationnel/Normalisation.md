# Preuve de Normalisation
## Personne
```MLD
Personne(
    #pseudo : String,
    email : String,
    nom : String,
    prenom : String,
    date_naissance : Date,
    lien_photo : String,
    mot_de_passe : String,
    numero : String,
    date_inscription : Date,
    date_derniere_connexion : Date,
    est_superadmin : booleen
)
avec email KEY, mot_de_passe NOT NULL
```
Les clés candidates sont pseudo et email.
Deux utilisateurs peuvent choisir de ne donner d'informations personnelles et donc avoir tous ces champs NULL et avoir le même mot de passe.

 * pseudo->email
 * pseudo->nom
 * pseudo->prenom
 * pseudo->date_naissance
 * pseudo->lien_photo
 * pseudo->mot_de_passe
 * pseudo->numero
 * pseudo->date_inscription
 * pseudo->date_derniere_connexion
 * pseudo->est_superadmin


 - email->pseudo (car email est une clé candidate)

Donc cette table est en BCNF.


## Groupe
```MLD
Groupe(
    #nom : String,
    description : String,
    createur => Personne.pseudo,
    parent => Groupe.nom,
    date_creation : Date,
    suppression => Personne.pseudo
) 
avec createur, date_creation NOT NULL
```
Seuls nom, createur et date_creation sont obligatoirement not null.

Deux groupes différents peuvent être créés le même jour.
Un créateur peut avoir créé plusieurs groupes, cette table est en BCNF.

## Adhesion
```MLD
Adhesion(
    #pseudo => Personne,
    #nom => Groupe,
    adhesion_validee : Boolean,
    date_demande : Date
)
avec date_demande, adhesion_validee NOT NULL
```
Rien ne peut être null dans cette relation.

Une personne peut demander à adhérer à plusieurs groupes. Donc pseudo pris seul ne peut déterminer les autres attributs.
de même, plusieurs personnes peuvent demander à adhérer à un même groupe. Donc groupe seul ne peut pas déterminer les autres attributs.

Donc (pseudo, nom) -> adhesion_validee, date_demande et la table est en BCNF.

## Administation
```MLD
Administration(#nom => Groupe.nom, #pseudo => Personne.pseudo)
```

C'est une relation toute clé dont les champs sont indépendants.
Un groupe peut être administré par plusieurs personnes. Une personne peut administrer plusieurs groupes.

La table est en BCNF.
## Bannissement
```MLD
Bannissement(
    #nom => Groupe.nom,
    #pseudo => Personne.pseudo,
    banni_par => Personne.pseudo,
    début_bannissement : Date,
    fin_bannissement : Date
)
avec banni_par, debut_banissement NOT NULL
```

Des personnes différentes peuvent être bannis à la même date par la même personne. Une personne peut être banni de plusieurs groupes. Un même groupe peut avoir banni plusieurs personnes.
Ainsi cette table est en BCNF.


## Lien
```MLD
Lien(#description: String)
```
Relation à un attribut, BCNF.

## Relation
```MLD
Relation(
    #personne1 => Personne.pseudo,
    #personne2 => Personne.pseudo,
    type_lien => Lien.description
)
avec type_lien NOT NULL
```
Aucun attribut ne peut être null.

On a choisi d'ignorer les doubles relations comme Collègue et Frère en même temps.

On peut avoir le même type de lien avec 2 personnes différentes (exemple : être collègue avec Alexandre et être collègue avec Louis en même temps).

Cette table est en BCNF.


## Formation
```MLD
Formation(
    #nom : String,
    niveau : String, 
    secteur : String,
    duree : int,
    description : String
)
```
Tout peut être null sauf nom. Cela revient à un cas d'une table à un attribut donc la table est BCNF.

## SuiviDeFormation
```MLD
SuiviDeFormation(
    #pseudo => Personne,
    #nom => Formation,
    début_suivi : Date,
    spécialité : String,
    mention : String,
    commentaire : String
)
```
Tout peut être null sauf pseudo et nom. Une personne peut avoir suivi différentes formations et une formation peut avoir été suivie par différentes personnes.
Donc pas de dépendence fonctionnelle entre pseudo et le nom de la formation et la table est en BCNF.

## Compétence
```MLD
Compétence(#nom : String, description : String)
```
La description peut être null, cela se rapporte à un cas d'une table à 1 seul attribut. 

On a seulement nom -> description. C'est donc une forme BCNF.
## Expertise
```MLD
Expertise(
    #pseudo => Personne,
    #nom => Compétence,
    niveau : int,
    commentaire : String
)
avec niveau compris entre 0 et 5 et NOT NULL.

```
Le commentaire peut être null. Une compétence peut être maitrisé par plusieurs personnes (avec le même niveau ou un niveau différent). Une personne peut maitriser plusieurs compétences.
Une personne peut avoir le même niveau dans plusieurs compétences.
Donc (pseudo, nom)->niveau, commentaire la table est BCNF.

## Entreprise
```MLD
Entreprise(
    #siren : int,
    nom : String,
    secteur : String,
    nb_employes : int,
    pays : String,
    logo_url : String
)
avec nom NOT NULL
```
Plusieurs entreprises peuvent avoir le même nom.

La relation est en BCNF.

## Experience
```MLD
Experience(
    #pseudo => Personne,
    #siren => Entreprise,
    #début : Date,
    fin : Date,
    poste : String,
    description : String
)
avec poste NOT NULL
```
Plusieurs personnes peuvent avoir été embauchés le même jour.
Plusieurs personnes travaillent dans une même entreprise.
Plusieurs personnes peuvent avoir été embauchés le même jour dans une même entreprise.
Une personne peut avoir été embauché 2 fois dans la même entreprise à 2 dates différentes (au même poste).
On considère qu'une personne peut avoir été embauché le même jour dans 2 entreprises différentes (exemple : il est employé dans 2 entreprises d'un même groupe, le même jour).
Avec ces considérations, la table est en BCNF.

## Type
```MLD
Type(#nom : String)
```
Un seul attribut donc pas de dépendence fonctionne. On a à faire à une table BCNF.
## Message
```MLD
Message(
    #numero : int,
    auteur => Personne.pseudo,
    date_publication : DateTime,
    groupe => Groupe.nom,
    titre : String,
    contenu : String,
    est_commentable : booléen,
    supprimé : booléen,
    type => Type.nom,
    parent => Message.numero,
    date_derniere_modification : DateTime
) 
avec type, contenu NOT NULL, est_commentable, supprimé NOT NULL, groupe NOT NULL, avec (auteur, date_publication) clé candidate.
```
Cette relation possède 2 clés : numero, et (auteur, date_publication).
Une personne peut publier plusieurs messages à des moments différents.
Au même moment, plusieurs messages peuvent être posté par des personnes différentes.
2 messages postés par des différentes personnes peuvent avoir exactement le même contenu (exemple : "ok").
Une personne ne peut pas poster au même moment 2 messages.

Cette relation est en BCNF.


## Like
```MLD
Aime(
    #pseudo => Personne,
    #message => Message.numero
)

```
Relation toute clé.
Une personne peut liker plusieurs messages différents.
Un même message peut être liké par plusieurs personnes.

C'est une relation BCNF.
## Dossier
```MLD
Dossier(#nom : String, description : String)
```
La description peut être null, on se rapporte alors à un cas d'une table à un seul attribut.
Il s'agit donc d'une relation BCNF.

## Tag
```MLD
Tags(#numero => Message, #nom => Dossier)
```
Un message peut avoir plusieurs tags donc peut apparaitre dans plusieurs dossiers.
Un dossier peut posséder plusieurs messages. Il s'agit donc d'une relation BCNF.

# Conclusion

Puisque toutes les tables sont BCNF, la base est elle même BCNF (et donc 3NF).