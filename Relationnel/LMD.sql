-- Fonctions de recherche basiques
        -- Rechercher une personne
SELECT pseudo, nom, prenom, date_naissance, lien_photo, date_inscription, email FROM Personne WHERE nom = 'chaine_recherchee' OR prenom = 'chaine_recherchee' OR pseudo = 'chaine_recherchee';
        -- Afficher son profil complet
SELECT Personne.pseudo, Personne.nom, Personne.prenom, Personne.date_naissance, Personne.lien_photo, Personne.date_inscription, Personne.email, SuiviDeFormation.nom, SuiviDeFormation.debut_suivi, SuiviDeFormation.specialite, SuiviDeFormation.mention FROM Personne INNER JOIN SuiviDeFormation ON Personne.pseudo = SuiviDeFormation.pseudo AND Personne.pseudo = 'pseudo';

-- Fonctions relatives aux groupes
SELECT COUNT(pseudo) FROM Adhesion WHERE nom='Linux'; -- Nombre de personne dans un groupe.
-- Rechercher un groupe, son nombre de membres et ses membres
SELECT nom, description, date_creation FROM Groupe WHERE nom = 'chaine_recherchee';
SELECT COUNT(*) FROM Adhesion WHERE nom = 'nom_groupe';
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Adhesion ON Personne.pseudo = Adhesion.pseudo WHERE Adhesion.nom = 'nom_groupe';

-- Fonction relative aux messages
SELECT auteur, date_publication, groupe, titre, contenu FROM Message INNER JOIN Aime ON Message.numero = Aime.message WHERE Aime.pseudo = 'Potiron'; -- Liste des messages aimés par un utilisateur
-- Nombre de j'aimes d'un message
SELECT COUNT(*) FROM Aime WHERE message = 1;
-- Classement des message les plus likés
SELECT message, COUNT(*) FROM Aime GROUP BY message ORDER BY COUNT(*) DESC;
        -- Rechercher une entreprise 
SELECT * FROM Entreprise WHERE nom = 'chaine_recherchee' OR siren = 123456789;

        -- Rechercher des personnes bannies jusqu'à une certaine date 
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Bannissement ON Personne.pseudo = Bannissement.pseudo WHERE Bannissement.fin_bannissement = '01/01/19';
        -- Rechercher des personnes bannies depuis une certaine date 
SELECT DISTINCT Personne.nom, Personne.prenom, Personne.lien_photo FROM Personne INNER JOIN Bannissement ON Personne.pseudo = Bannissement.pseudo WHERE Bannissement.debut_bannissement = '01/01/19';




-- Afficher le nombre de personnes dans le groupe JQuery 

SELECT count (*) FROM ADHESION WHERE nom = 'JQuery'; 

-- Afficher le nom, le prénom ainsi que la date de naissance des personnes dans le groupe Linux
SELECT P.nom, P.prenom, P.date_naissance FROM ADHESION A, Personne P WHERE A.nom = 'Linux' AND A.pseudo = P.pseudo GROUP BY P.nom, P.prenom, P.date_naissance;

-- Calculer le pseudo des personnes qui ont postés au moins 2 messages sur le groupe Linux 
SELECT M.auteur, count (*) FROM Message M WHERE M.groupe = 'Linux'  GROUP BY M.auteur  HAVING count(*) > 1;

-- Afficher le nombre des messages commentables postés par Lucas Dziurzik sur l'ensemble des groupes 
SELECT M.groupe, count (*) FROM Message M, Personne P WHERE P.nom = 'Dziurzik'  AND P.prenom = 'Lucas' AND P.pseudo = M.auteur AND M.est_commentable IS TRUE  GROUP BY M.groupe;

-- Afficher le nombre de messages aimés par toutes les personnes donc le prénom commence par la lettre 'L'
SELECT count(*) FROM Personne P, Aime A WHERE P.prenom LIKE 'L%' AND P.pseudo = A.pseudo; 

-- Afficher le nom des entreprises et leur secteur dans laquelle la personne avec le pseudo 'La_Mouche' a travaillé
SELECT E.nom, E.secteur FROM Entreprise E, Experience Exp WHERE Exp.pseudo = 'La_Mouche' AND Exp.siren = E.siren;

-- Afficher l'intitulé des messages correspondant aux tags dans le dossier GIT
SELECT M.contenu FROM Message M, Tags T WHERE T.nom ='Git' AND T.message = M.numero;

-- Affichez la retlation entre 'Potiron' et 'Numero 1'
SELECT R.type_lien FROM  Relation R WHERE R.personne1 = 'Potiron'  AND R.personne2 = 'CompiZoo' ;



