1) Afficher le nombre de personnes dans le groupe JQuery 

SELECT count (*)
FROM ADHESION
WHERE nom = 'JQuery'; 

2) Afficher le nom, le prénom ainsi que la date de naissance des personnes dans le groupe Linux
SELECT P.nom, P.prenom, P.date_naissance
FROM ADHESION A, Personne P
WHERE A.nom = 'Linux'
AND A.pseudo = P.pseudo
GROUP BY P.nom, P.prenom, P.date_naissance;

3) Calculer le pseudo des personnes qui ont postés au moins 2 messages sur le groupe Linux 
SELECT M.auteur, count (*)
FROM Message M
WHERE M.groupe = 'Linux' 
GROUP BY M.auteur 
HAVING count(*) > 1;

4) Afficher le nombre des messages commentables postés par Lucas Dziurzik sur l'ensemble des groupes 
SELECT M.groupe, count (*)
FROM Message M, Personne P
WHERE P.nom = 'Dziurzik' 
AND P.prenom = 'Lucas'
AND P.pseudo = M.auteur
AND M.est_commentable IS TRUE 
GROUP BY M.groupe;

5) Afficher le nombre de messages aimés par toutes les personnes donc le prénom commence par la lettre 'L'
SELECT count(*)
FROM Personne P, Aime A
WHERE P.prenom LIKE 'L%'
AND P.pseudo = A.pseudo; 

6) Afficher le nom des entreprises et leur secteur dans laquelle la personne avec le pseudo 'La_Mouche' a travaillé
SELECT E.nom, E.secteur
FROM Entreprise E, Experience Exp
WHERE Exp.pseudo = 'La_Mouche'
AND Exp.siren = E.siren;

7) Affichez l'intitulé des messages correspondant aux tags dans le dossier GIT
SELECT M.contenu
FROM Message M, Tags T
WHERE T.nom ='Git' 
AND T.message = M.numero;

8) Affichez la retlation entre 'Potiron' et 'Numero 1'
SELECT R.type_lien
FROM  Relation R
WHERE R.personne1 = 'Potiron' 
AND R.personne2 = 'CompiZoo' ;



